const jwt = require('jsonwebtoken');
const secret_key = "KapugeGaNare9876";

// Generating a token
module.exports.createAccessToken = (user) => {
	const user_data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(user_data, secret_key, {});
}

// Verifying a token
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if (typeof token === "undefined"){
		console.log("Token not found in headers");
		return res.send({auth: 'Failed, please include token in the header of the request.'})
	}

	// console.log(token);

	token = token.slice(7, token.length);

	// console.log(token)

	jwt.verify(token, secret_key, (error, decoded_token) => {
		if(error){
			console.log("Token verification error:", error);
			return res.send({
				auth: 'Failed',
				message: error.message
			})
		}

		// console.log("Decoded Token:", decoded_token);
		  req.user = decoded_token;
		  // console.log("Request User:", request.user);

		next();
	})
}

// Verifying if user is admin
module.exports.verifyAdmin = (request, response, next) => {
	// Checks the request.user object which comes from the 'verify' function and contains the user data.
	if(request.user.isAdmin){
		return next();
	}

	return response.send({
		auth: 'Failed',
		message: 'Action Forbidden'
	})
}