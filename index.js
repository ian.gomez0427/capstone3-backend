// Server variables
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();
const port = 4000;
const userRoutes = require('./routes/userRoutes.js');
const collectionRoutes = require('./routes/collectionRoutes.js');
const cartRoutes = require('./routes/cartRoutes.js');
const app = express();
const bodyParser = require('body-parser');
const auth = require('./auth');

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors()); // This will allow our hosted front-end app to send requests to this server

// Routes
app.use('/api/users', userRoutes);
app.use('/api/collections', collectionRoutes);
app.use('/api/cart', cartRoutes);
app.use('/uploads', express.static('uploads'));

// Database connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-gomez.k6bt02u.mongodb.net/B303-OnePieceCollections?retryWrites=true&w=majority`,
	{useNewUrlParser: true, useUnifiedTopology: true});

mongoose.connection.on('error', () => console.log("Can't connect to database."));
mongoose.connection.once('open', () => console.log('Connected to MongoDB!'));


// Since we're hosting this API in the cloud, the port to be used should be flexible hence, the use of the process.env.PORT which will take the port that the cloud server uses if the 'port' variable isn't available.
app.listen(process.env.PORT || port, () => {
	console.log(`One Piece Collections API is now running at localhost:${process.env.PORT || port}`);
});

app.use(bodyParser.json({ limit: '10mb' }));
app.use('/uploads', express.static('uploads'));

module.exports = app;