const Collection = require('../models/Collection.js');

module.exports.addCollection = async (request, response) => {
    
    const newCollectionData = {
    	category: request.body.category,
        name: request.body.name,
        description: request.body.description,
        price: request.body.price,
        imageUrl: request.body.imageUrl
    };

    if (request.file) {
        newCollectionData.imageUrl = request.file.filename;
    }

    try {
        const savedCollection = await Collection.create(newCollectionData);
        const imageUrlBase = 'http://localhost:4000';
        const imageUrlRelativePath = '/uploads/' + savedCollection.imageUrl; // Use the correct field name
        const imageUrl = imageUrlBase + imageUrlRelativePath;

        return response.send({ imageUrl: imageUrl });
    } catch (error) {
        console.error('Error saving collection:', error);
        response.send(false);
    }
};

module.exports.getAllCollections = (request, response) => {
	return Collection.find({}).then(result => {
		return response.send(result);
	})
}

module.exports.getAllActiveCollections = (request, response) => {
	return Collection.find({isActive: true}).then(result => {
		return response.send(result);
	})
}

module.exports.getCollection = (request, response) => {
	return Collection.findById(request.params.id).then(result => {
		return response.send(result);
	})
}

module.exports.updateCollection = (request, response) => {
    let updated_collection_details = {
    	category: request.body.category,
        name: request.body.name,
        description: request.body.description,
        price: request.body.price,
    };

    if (request.file) {
        // An image was included in the request, update the imageUrl
        updated_collection_details.imageUrl = request.file.filename;
    }

    return Collection.findByIdAndUpdate(request.params.id, updated_collection_details, { new: true })
        .then(collection => {
            if (!collection) {
                return response.status(404).send('Collection not found');
            }

            return response.send({
                message: 'This item has been updated successfully!',
                collection: collection
            });
        })
        .catch(error => {
            return response.status(500).send({
                message: error.message
            });
        });
};


module.exports.archiveCollection = (request, response) => {
	return Collection.findByIdAndUpdate(request.params.id, { isActive: false }).then((collection, error) => {
		if(error){
			return response.send(false);
		}

		return response.send(true);
	})
}

module.exports.activateCollection = (request, response) => {
	return Collection.findByIdAndUpdate(request.params.id, { isActive: true }).then((collection, error) => {
		if(error){
			return response.send(false);
		}

		return response.send(true);
	})
}

module.exports.searchCollections = (request, response) => {
      const collectionName = request.body.collectionName;

      return Collection.find({ name: { $regex: collectionName, $options: 'i' } }).then((collections) => {
      	response.send(collections)
      }).catch(error => response.send({
      	message: error.message
      }))
}