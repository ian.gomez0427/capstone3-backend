const User = require('../models/User.js');
const Collection = require('../models/Collection.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then((result, error) => {
		if(error){
			return {
				message: error.message 
			}
		}

		if (result.length <= 0){
			return false;
		}

		// This will only return true if there are no errors AND there is an existing user from the database.
		return true;
	})
}

module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		mobileNo: request_body.mobileNo,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((registered_user, error) => {
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully registered a user!'
		};
	}).catch(error => console.log(error));
}

module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		// Checks if a user is found with an existing email
		if(result == null){
			return response.send({
				message: "The user isn't registered yet."
			}) 
		}

		// If a user was found with an existing email, then check if the password of that user matched the input from the request body
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			// If the password comparison returns true, then respond with the newly generated JWT access token.
			return response.send({
				accessToken: auth.createAccessToken(result),
				userId: result._id
			});
		} else {
			return response.send({
				message: 'Your password is incorrect.'
			})
		}
	}).catch(error => response.send(error));
}

module.exports.getProfile = (request_body) => {
	return User.findOne({_id: request_body.id}).then((user, error) => {
		if(error){
			return {
				message: error.message 
			}
		}

		user.password = "";

		return user;
	}).catch(error => console.log(error));
}

module.exports.enroll = async (request, response) => {
	// Blocks this function if the user is an admin
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}

	// [SECTION] Updating User Collection
	// This variable will return true once the user data has been updated
	let isUserUpdated = await User.findById(request.user.id).then(user => {

		let new_enrollment = {
			collectionId: request.body.collectionId
		}

		user.enrollments.push(new_enrollment);

		return user.save().then(updated_user => true).catch(error => error.message);
	})

	// Sends any error within 'isUserUpdated' as a response
	if(isUserUpdated !== true){
		return response.send({ message: isUserUpdated });
	}

	// [SECTION] Updating collection collection
	let isCollectionUpdated = await Collection.findById(request.body.collectionId).then(collection => {
		let new_enrollee = {
			userId: request.user.id
		}

		collection.enrollees.push(new_enrollee);

		return collection.save().then(updated_collection => true).catch(error => error.message)
	})


	if(isCollectionUpdated !== true){
		return response.send({ message: isCollectionUpdated });
	}

	// [SECTION] Once isUserUpdated AND isCCollectionUpdated return true
	if(isUserUpdated && isCollectionUpdated){
		return response.send({ message: 'Enrolled successfully!' });
	}
}

module.exports.getEnrollments = (request, response) => {
	User.findById(request.user.id)
		.then(user => response.send(user.enrollments))
		.catch(error => response.send(error.message))
}