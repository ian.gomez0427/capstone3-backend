const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const User = require("../models/User.js");

module.exports.placeOrder = async (userEmail, orderData) => {
  const { products } = orderData;

  let totalAmount = 0;

  try {
    for (const item of products) {
      const product = await Product.findById(item.productId);
      if (!product) {
        throw new Error("Invalid product ID");
      }
      
      if (product.availableItems < item.quantity) {
        throw new Error("Insufficient available items");
      }

      const subtotal = product.price * item.quantity;
      totalAmount += subtotal;
    }

    // Create a new order and save it
    const newOrder = new Order({
      products: products,
      totalAmount: totalAmount,
      purchasedBy: userEmail
    });

    return newOrder.save()
      .then(savedOrder => {
        return { message: "Order placed successfully", data: savedOrder };
      })
      .catch(error => {
        throw new Error("An error occurred while saving the order");
      });
  } catch (error) {
    return { error: error.message };
  }
};


module.exports.viewOrdersByUser = (userEmail) => {
  return Order.find({ purchasedBy: userEmail }) // Use purchasedBy field
    .then(orders => {
      return { data: orders };
    })
    .catch(error => {
      return { error: "An error occurred while retrieving orders." };
    });
};


module.exports.viewAllOrders = () => {
  return Order.find()
    .then(orders => {
      return { data: orders };
    })
    .catch(error => {
      return { error: "An error occurred while retrieving orders." };
    });
};


module.exports.addToCart = (userId, productId, quantity) => {
  // Verify that the product exists and has sufficient available items
  return Product.findById(productId)
    .then(product => {
      if (!product) {
        return { error: 'Product not found' };
      }
      if (product.availableItems < quantity) {
        return { error: 'Insufficient available items' };
      }

      // Access the user's cart from req.user and update it
      const user = req.user; // Assuming req.user is populated by the auth.verify middleware

      // Create or update the user's cart
      if (!user.cart) {
        user.cart = [];
      }

      // Check if the product is already in the cart
      const existingCartItem = user.cart.find(item => item.productId.toString() === productId);
      if (existingCartItem) {
        // Update the quantity if the product is already in the cart
        existingCartItem.quantity += quantity;
      } else {
        // Add a new cart item if the product is not in the cart
        user.cart.push({ productId, quantity });
      }

      // Update the user's cart and save
      return user.save().then(savedUser => {
        return { message: 'Product added to cart successfully', data: savedUser.cart };
      });
    })
    .catch(error => {
      return { error: 'An error occurred while adding to cart' };
    });
};


// module.exports.addCourse = (request, response) => {
//   let new_course = new Course({
//     name: request.body.name,
//     description: request.body.description,
//     price: request.body.price
//   });

//   return new_course.save().then((saved_course, error) => {
//     if(error){
//       return response.send(false);
//     }

//     return response.send(true);
//   }).catch(error => response.send(error));
// }


// module.exports.getAllCourses = (request, response) => {
//   return Course.find({}).then(result => {
//     return response.send(result);
//   })
// }

// module.exports.getAllActiveCourses = (request, response) => {
//   return Course.find({isActive: true}).then (result => {
//     return response.send(result);
//   })
// }

// module.exports.getCourse = (request, response) => {
//   return Course.findById(request.params.id).then(result => {

//     return response.send(result);
//   }).catch(error => {
//             console.error(error);
//             // return response.status(500).json({ error: 'Course not found' });
//             return response.status(500).send('Course not found');
//         });

// }

// module.exports.updateCourse = (request, response) => {
//   let updated_course_details = {
//     name: request.body.name,
//     description: request.body.description,
//     price: request.body.price
//   };

//   return Course.findByIdAndUpdate(request.params.id, updated_course_details).then((course,error) => {
//     if (error){
//       return response.send({
//         message: error.message
//       })
//     }
//     return response.send({
//       message: 'Course has been updated successfully!'
//     })
//   })
// }

// module.exports.archiveCourse = (request, response) => {
//     return Course.findByIdAndUpdate(request.params.id, { isActive: 'false' }).then((course,error) => {
//     if (error){
//       return response.send({
//       message: 'Failed to Archive!'
//     })
//     }
//     return response.send({
//       message: 'Course has been archived successfully!'
//     })
//   })
// }

// module.exports.activateCourse = (request, response) => {
//     return Course.findByIdAndUpdate(request.params.id, { isActive: 'true' }).then((course,error) => {
//     if (error){
//       return response.send({
//       message: 'Failed to Activate!'
//     })
//     }
//     return response.send({
//       message: 'Course has been activated successfully!'
//     })
//   })
// }


// //////////////////////////
// module.exports.searchCoursesByName = (request, response) => {
//   const { courseName } = request.body;
  
//   Course.find({ name: { $regex: courseName, $options: 'i' } })
//     .then(courses => {
//       if (courses.length === 0) {
//         return response.status(404).send('Course not found');
//       }
//       return response.send(courses);
//     })
//     .catch(error => {
//       console.error(error);
//       return response.status(500).send('Search error');
//     });
// };

