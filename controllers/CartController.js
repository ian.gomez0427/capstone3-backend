const Cart = require("../models/Cart.js");
const Collection = require("../models/Collection.js");


module.exports.viewCart = (userId) => {
  return Cart.findOne({ userId: userId })
    .populate('items.collectionId') // Populate the collection details
    .then(cart => {
      if (!cart || cart.items.length === 0) {
        return { message: 'Found No Collection on your Cart' };
      }

      // Calculate subtotal for each item and total amount
      cart.items.forEach(item => {
        item.subtotal = item.quantity * item.collectionId.price;
      });

      const totalAmount = cart.items.reduce((total, item) => total + item.subtotal, 0);

      return {
        data: {
          cartItems: cart.items,
          totalAmount: totalAmount
        }
      };
    })
    .catch(error => {
      return { error: 'An error occurred while retrieving the cart.' };
    });
};


module.exports.addCollectionToCart = (userId, collectionId, quantity=1) => {

  console.log('Received Collection ID:', collectionId);
  return Collection.findById(collectionId)
    .then(collection => {
      if (!collection) {
        return { error: 'One Piece Collection not found' };
      }

      const subtotal = collection.price * quantity;

      return Cart.findOne({ userId })
        .then(cart => {
          if (!cart) {
            const newCart = new Cart({
              userId,
              items: [{ collectionId, quantity, subtotal }],
              totalAmount: subtotal
            });
            return newCart.save();
          }

          const existingItem = cart.items.find(item => item.collectionId.equals(collectionId));
                    if (existingItem) {
                        existingItem.quantity++; // Increment the quantity
                        cart.totalAmount += subtotal;
                    } else {
                        cart.items.push({ collectionId, quantity: 1, subtotal });
                        cart.totalAmount += subtotal;
                    }

          return cart.save();
        })
        .then(savedCart => {
          return { message: 'Collection added to cart successfully', data: savedCart };
        });
    })
    .catch(error => {
      return { error: 'An error occurred while adding the collection to the cart.' };
    });
};

module.exports.editCartItem = (userId, cartItemId, updatedQuantity) => {
  if (!cartItemId || !updatedQuantity || updatedQuantity <= 0) {
    return { error: 'Issue on the input data' };
  }

  return Cart.findOne({ userId })
    .populate('items.collectionId')
    .then(cart => {
      if (!cart) {
        return { error: 'No Collections placed on your Cart' };
      }

      const cartItem = cart.items.find(item => item._id.equals(cartItemId));
      if (!cartItem) {
        return { error: 'Cart item not found' };
      }

      const collectionPrice = cartItem.collectionId.price;
      cart.totalAmount -= cartItem.subtotal;

      cartItem.quantity = updatedQuantity;
      cartItem.subtotal = collectionPrice * updatedQuantity;
      // console.log('New Subtotal:', cartItem.subtotal);
      cart.totalAmount += cartItem.subtotal;
      // console.log('New Total Amount:', cart.totalAmount);

      return cart.save()
        .then(savedCart => {
          return { message: 'Cart item updated successfully', data: savedCart };
        });
    })
    .catch(error => {
      // console.error('Error:', error);
      return { error: 'An error occurred while updating the cart item.' };
    });
};


module.exports.removeCartItem = (userId, collectionId) => {
  if (!collectionId) {
    return { error: 'Enter the Collection ID you want to delete from your cart' };
  }

  return Cart.findOneAndUpdate(
    { userId },
    { $pull: { items: { collectionId } } },
    { new: true }
  )
    .then(updatedCart => {
      if (!updatedCart) {
        return { error: 'No Collections placed on your Cart' };
      }
      return { message: 'Cart item removed successfully', data: updatedCart };
    })
    .catch(error => {
      return { error: 'An error occurred while removing the cart item.' };
    });
};


