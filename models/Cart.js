const mongoose = require('mongoose');

const cartItemSchema = new mongoose.Schema({

  collectionId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Collections',
    required: true
  },

  quantity: {
    type: Number,
    required: true
  },

  subtotal: {
    type: Number,
    required: true,
  },

});

const cartSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',

  },
  items: [cartItemSchema],
  totalAmount: {
    type: Number,
    required: true
  }
});


module.exports = mongoose.model('Cart', cartSchema);