const mongoose = require('mongoose');

const collection_schema = new mongoose.Schema({
    
    category: {
        type: String, 
        required: [true, "Category is required"]
    },

	name : {
        type : String,
        required : [true, "Item Name is required"]
    },

    description : {
        type : String,
        required : [true, "Please add some description."]
    },

    price : {
        type : Number,
        required : [true, "Price is required"]
    },

    isActive : {
        type : Boolean,
        default : true
    },

    createdOn: {
    type: Date,
    default: Date.now
    },

    imageUrl : {
        type : String,
        required : [true, "Please upload a picture."]
    },

    enrollees : [
        {
            userId : {
                type : String,
                required: [true, "UserId is required"]
            },
            enrolledOn : {
                type : Date,
                default : new Date() 
            }
        }
    ]
})

module.exports = mongoose.model("Collection", collection_schema);