const express = require('express')
const router = express.Router();
const auth = require('../auth.js');
const multer = require('multer');
const CollectionController = require('../controllers/CollectionController.js');

// You can destructure the 'auth' variable to extract the function being exported from it. You can then use the functions directly without having to use dot (.) notation.
// const {verify, verifyAdmin} = auth;

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/'); // Specify the upload directory
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    cb(null, file.fieldname + '-' + uniqueSuffix);
  },
});

const upload = multer({
  storage: storage,
  limits: { fileSize: 10 * 1024 * 1024 } // Adjust as needed
});

// Create single collection
router.post('/', auth.verify, auth.verifyAdmin, upload.single('image'), (request, response) => {
	CollectionController.addCollection(request, response)
});

// Get all collections
router.get('/all', (request, response) => {
	CollectionController.getAllCollections(request, response);
});

// Get all active collections
router.get('/', (request, response) => {
	CollectionController.getAllActiveCollections(request, response);
})


// Get single collection
router.get('/:id', (request, response) => {
	CollectionController.getCollection(request, response);
})

// Update single collection
// router.put('/:id', auth.verify, auth.verifyAdmin, upload.single('image'), (request, response) => {
// 	CollectionController.updateCollection(request, response);
// })
router.put('/:id', auth.verify, auth.verifyAdmin, upload.single('image'), CollectionController.updateCollection);


// Archive single collection
router.put('/:id/archive', auth.verify, (request, response) => {
	CollectionController.archiveCollection(request, response);
})

// Activate single collection
router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
	CollectionController.activateCollection(request, response);
})

// Search collection by name
router.post('/search', (request, response) => {
	CollectionController.searchCollections(request, response);
});

module.exports = router;