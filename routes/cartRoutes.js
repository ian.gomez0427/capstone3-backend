const express = require('express');
const router = express.Router();
const auth = require('../auth.js');
const CartController = require('../controllers/CartController.js');

// View Cart
router.get('/view-cart', auth.verify, (req, res) => {
  const userId = req.user._id;
  
  CartController.viewCart(userId)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while retrieving the cart.' });
    });
});


// Add Collection to Cart
// cartRoute.js

router.post('/add-to-cart', auth.verify, (req, res) => {
  const token = req.headers.authorization;
  console.log('Received Token:', token); // Log the token
  const { collectionId } = req.body;

  console.log('User ID:', req.user._id); // Log the user ID
  console.log('Received Collection ID:', collectionId); // Log the received collection ID

  CartController.addCollectionToCart(req.user._id, collectionId) // Modify this line
    .then(result => {
      console.log('Cart Addition Result:', result); // Log the result
      res.send(result);
    })
    .catch(error => {
      console.error('Cart Addition Error:', error); // Log the error
      res.status(500).json({ error: 'An error occurred while adding the collection to the cart.' });
    });
});

// Edit Cart Item
router.put('/editCart', auth.verify, (req, res) => {
  const userId = req.user._id;
  const { cartItemId, updatedQuantity } = req.body; // Use lowercase property names

  CartController.editCartItem(userId, cartItemId, updatedQuantity)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while updating the cart item.' });
    });
});

router.delete('/removeFromCart/:collectionId', auth.verify, (req, res) => {
  const userId = req.user._id;
  const collectionId = req.params.collectionId;

  CartController.removeCartItem(userId, collectionId)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while removing the cart item.' });
    });
});


module.exports = router;
