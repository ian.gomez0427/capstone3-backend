const express = require('express')
const router = express.Router();
const auth = require('../auth.js');
const OrderController = require('../controllers/OrderController.js');


router.post('/placeOrder', auth.verify, (req, res) => {
  OrderController.placeOrder(req.user.email, req.body)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while placing the order.' });
    });
});

router.get('/viewOrders', auth.verify, (req, res) => {
  OrderController.viewOrdersByUser(req.user.email) // Use req.user.email instead of req.user._id
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while retrieving orders.' });
    });
});

router.get('/viewAllOrders', auth.verify, auth.verifyAdmin, (req, res) => {
  OrderController.viewAllOrders()
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while retrieving orders.' });
    });
});


router.post('/addToCart', auth.verify, (req, res) => {
  OrderController.addToCart(req.user._id, req.body.productId, req.body.quantity)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).json({ error: 'An error occurred while adding to cart.' });
    });
});


module.exports = router;
// // You can destructure the 'auth' variable to extract the function being exported from it. You can then use the functions directly without having to use dot (.) notation.
// // const {verify, verifyAdmin} = auth;

// // Insert routes here

// // Create single post
// router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
//   CourseController.addCourse(request, response)
// });


// // Get all courses
// router.get('/all', (request, response) => {
//   CourseController.getAllCourses (request, response);
// })

// // get all active courses
// router.get('/', (request, response) => {
//   CourseController.getAllActiveCourses(request, response);
// })

// // get single course
// router.get('/:id', (request, response) => {
//   CourseController.getCourse(request, response);
// })

// // update single course
// router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
//   CourseController.updateCourse(request, response);
// })

// // archive single course
// router.put('/:id/archive', auth.verify, (request, response) => {
//   CourseController.archiveCourse(request, response);
// })

// // activate single course
// router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
//   CourseController.activateCourse(request, response);
// })

// // find Course by name
// router.post('/search', auth.verify, (request, response) => {
//   CourseController.searchCoursesByName(request, response);
// })



